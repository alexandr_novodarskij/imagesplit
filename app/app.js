angular.module('imageSplitApp', ['flow'])
    .config(['flowFactoryProvider', function(flowFactoryProvider) {
        flowFactoryProvider.defaults = {
            target: "http://127.0.0.1:6969/upload",
            permanentErrors: [404, 500, 501],
            testChunks: false,
            maxChunkRetries: 1,
            chunkRetryInterval: 5000,
            simultaneousUploads: 4,
            singleFile: true
        };
        // flowFactoryProvider.on('catchAll', function(event) {
        //     console.log('catchAll', arguments);
        // });
    }])
    .constant('Config', {
        serverAddress: "http://127.0.0.1:6969",
    })
    .factory('DataFactory', function($http, Config) {
        var DataFactory = {};

        DataFactory.getSplit = function(data) {
            var params = '';
            angular.forEach(data, function(value, key) {
                if (value != "") {
                    params += '&' + key + '=' + value;
                }
            });
            return $http.get(Config.serverAddress + "/upload?" + params)
        }
        DataFactory.getUploadsAll = function() {
            return $http.get(Config.serverAddress + "/upload/all/")
        }
        return DataFactory;
    })
    .controller('MainCtrl', function($scope, DataFactory) {
        $scope.opt = {
            countSplit: 5,
        }
        $scope.getSplit = function(ind) {
          if (typeof ind != 'undefined') {
            console.log($scope.imgsAll[ind].file);
            $scope.file = $scope.imgsAll[ind].file
          }
            if (typeof $scope.file != "undefined") {
                $scope.file.count = $scope.opt.countSplit;
                DataFactory.getSplit($scope.file).then(function(resp) {
                    $scope.displayRes(JSON.stringify(resp.data))
                }, function(err) {
                    console.log(err);
                })

            }
        }
        $scope.getUploadsAll = function() {
          DataFactory.getUploadsAll().then(function (res) {
            if (res.data.status) {
                imgs = []
                $scope.imgsAll = res.data.imgs;
            }
          },function (err) {
            console.log(err);
          })
        }
        $scope.getUploadsAll()
        $scope.displayRes = function(message) {
            $scope.message = JSON.parse(message);
            if (typeof $scope.message.file != 'undefined') {
                $scope.file = $scope.message.file;
            }

            if ($scope.message.status) {
                img = []
                console.log($scope.message);
                for (var i = 0; i < $scope.message.count; i++) {
                    img.push('/app/uploads/splited/' + $scope.message.path + "/" + i + $scope.message.ext + "?" + Date.now())
                }
                $scope.imgs = img;
                $scope.getUploadsAll()

            }
        }
    });
