var exec = require('child_process').exec;
var fs = require('fs-extra')

exports.imgsplit = function(options, callback) {
    var srcPath = typeof options.srcPath !== 'undefined' ? options.srcPath : null;
    var count = typeof options.count !== 'undefined' ? options.count : null;
    var name = typeof options.name !== 'undefined' ? options.name : null;
    var desPath = typeof options.desPath !== 'undefined' ? options.desPath : null;
    if ((srcPath) && (count)) {
        exec("identify -format \"%w,%h\" \"" + srcPath + "\"", function(error, stdout, stderr) {
            if (error) {
                console.log(error);
                return null;
            }
            wh = stdout.split(",");
            step = wh[0] / count
            console.log([step, count, wh]);
            if (!fs.existsSync(desPath + name.slice(0, name.lastIndexOf('.')))) {
                fs.mkdirsSync(desPath + name.slice(0, name.lastIndexOf('.')));
            }
            var i = 0
            var k = 0
            for (i = 0; i * step < wh[0]; i++) {
                exec("convert \"" + srcPath + "\" -crop " + step + "x" + wh[1] + "+" + i * step + "+0 \"" + desPath + name.slice(0, name.lastIndexOf('.')) + "\"/" + i + "" + name.slice(name.lastIndexOf('.')), function(error, stdout, stderr) {
                    if (error) {
                        return 0
                    }
                    k += 1;
                    if (k == count - 1) {
                        callback(name.slice(0, name.lastIndexOf('.')), count, name.slice(name.lastIndexOf('.')))
                    }
                })
            }
            //  callback(name.slice(0,name.lastIndexOf('.')), i, name.slice(name.lastIndexOf('.')))
        });
    }
}
