var request = require('request'),
    cors = require('cors'),
    express = require('express'),
    multer = require('multer'),
    mongoose = require('mongoose'),
    im = require(__dirname + "/" + "imgsplit/imgsplit"),
    storage = multer.diskStorage({
        destination: function(req, file, cb) {
            cb(null, './app/uploads')
        },
        filename: function(req, file, cb) {
            cb(null, Date.now() + "-" + file.originalname)
        }
    }),
    upload = multer({
        storage: storage
    }),
    app = express();
    mongoose.connect('mongodb://localhost/imgApp');
    var ImgSchema = new mongoose.Schema({
      file: {
        fieldname: String,
        originalname: String,
        encoding: String,
        mimetype: String,
        destination: String,
        filename: String,
        path: String,
        size: Number
      },
      updated_at: { type: Date, default: Date.now },
    });
    var Img = mongoose.model('Img', ImgSchema);
// app.use(express.queryParser());
app.use(cors())
app.use('/app', express.static('./app'))


app.route('/')
    .get(function(req, res) {
        res.sendFile(__dirname + "/app/index.html")
    })

app.post('/upload', upload.any(), function(req, res, next) {
    var count = 1
    if ((typeof req.body.count != 'undefined') && (req.body.count > 0)) {
        count = req.body.count
    }
    var img = new Img({
      file: req.files[0]
    });
    img.save(function(err) {
      if (err) throw err;
    });
    im.imgsplit({
        srcPath: __dirname + "/" + req.files[0].path,
        desPath: __dirname + "/app/uploads/splited/",
        count: count,
        name: req.files[0].originalname
    }, function(path, count, ext) {
        res.json({
            status: true,
            file: req.files[0],
            path: path,
            count: count,
            ext: ext
        });
    })
})
app.get('/upload/all', function(req, res, next) {
  Img.find({}, function(err, imgs) {
    if (err) throw err;
    res.json({
      status: true,
      imgs: imgs
    })
  });
})
app.get('/upload', function(req, res, next) {
    var count = 1
    if ((typeof req.query.count != 'undefined') && (req.query.count > 0)) {
        count = req.query.count
    }
    im.imgsplit({
        srcPath: __dirname + "/" + req.query.path,
        desPath: __dirname + "/app/uploads/splited/",
        count: count,
        name: req.query.originalname
    }, function(path, count, ext) {
        console.log(path, count, ext);
        res.json({
            status: true,
            path: path,
            count: count,
            ext: ext
        });
    })

})
app.listen(6969, function() {
    console.log('Example app listening on port 6969!');
});
